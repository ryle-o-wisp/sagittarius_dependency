#!/bin/bash

if [ -z "${ANDROID_NDK_HOME}" ]
then
    >2& echo "ANDROID_NDK_HOME is not specified"
    exit 1
fi

MAKE_STANDALONE_TOOLCHAIN="${ANDROID_NDK_HOME}/build/tools/make-standalone-toolchain.sh"

if [ ! -f "${MAKE_STANDALONE_TOOLCHAIN}" ]
then
    >2% echo "make-standalone-toolchain.sh file is not found from ${MAKE_STANDALONE_TOOLCHAIN}"
    exit 2
fi

ARCH=$1 # expected arm, arm64, x86, mips
MAKE_ANDROID_PLATFORM=$2 # expected android-{n}
MAKE_ANDROID_PLATFORM=${MAKE_ANDROID_PLATFORM:-"android-21"}
EXPORT_ANDROID_NDK_STANDALONE_TOOLCHAIN_ROOT=$3
EXPORT_ANDROID_NDK_STANDALONE_TOOLCHAIN_ROOT=${EXPORT_ANDROID_NDK_STANDALONE_TOOLCHAIN_ROOT:-".cache/ndk_standalone_toolchain/${ARCH}"}

bash "${MAKE_STANDALONE_TOOLCHAIN}" --arch="${ARCH}" --platform="${MAKE_ANDROID_PLATFORM}" --install-dir=${EXPORT_ANDROID_NDK_STANDALONE_TOOLCHAIN_ROOT}