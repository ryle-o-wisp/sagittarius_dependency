#!/usr/bin/bash

XCODEBUILD=${XCODEBUILD:-"xcodebuild"}
BREAKPAD_PATH=$1
SDK=$2 # expected iphoneos, iphoneossimulator
CONFIGURATION=$3 # expected Release, Debug

${XCODEBUILD} -project ${BREAKPAD_PATH}/src/client/ios/Breakpad.xcodeproj -sdk ${SDK} -configuration ${CONFIGURATION}