#!/bin/bash

BREAKPAD_HOST=$1 # expected arm-linux-androideabi, arm64, x86, mips
COMPILER_PREFIX=$2
BREAKPAD_PATH=$3
NDK_STANDALONE_TOOLCHAIN_PATH=$4

export CC="${COMPILER_PREFIX}-gcc"
export CXX="${COMPILER_PREFIX}-g++"
export CFLAGS="-fpermissive"
export CXXFLAGS="-fpermissive"
export SYSROOT="${NDK_STANDALONE_TOOLCHAIN_PATH}/sysroot"
export PATH="${NDK_STANDALONE_TOOLCHAIN_PATH}/bin:${PATH}"

${CC} --version

PATH_BEFORE=`pwd`
cd "${BREAKPAD_PATH}/src"
echo "Building breakpad with NDK at `pwd`"
export
pwd
../configure --host=${BREAKPAD_HOST} --disable-processor --disable-tools
make -j4
cd "${PATH_BEFORE}"