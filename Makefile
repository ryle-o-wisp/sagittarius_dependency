#
# Sagittarius engine dependency libraries Makefile
# Copyright(c) 2018 Donghyun-You
#

BGFX_ANDROID_ARM_RELEASE_BUILD_PATH=".build/bgfx_android_arm_release"
BGFX_ANDROID_ARM_DEBUG_BUILD_PATH=".build/bgfx_android_arm_debug"
BGFX_ANDROID_X86_RELEASE_BUILD_PATH=".build/bgfx_android_x86_release"
BGFX_ANDROID_X86_DEBUG_BUILD_PATH=".build/bgfx_android_x86_debug"
BGFX_IOS_ARM_RELEASE_BUILD_PATH=".build/bgfx_ios_arm_release"
BGFX_IOS_ARM_DEBUG_BUILD_PATH=".build/bgfx_ios_arm_debug"
BGFX_IOS_ARM64_RELEASE_BUILD_PATH=".build/bgfx_ios_arm64_release"
BGFX_IOS_ARM64_DEBUG_BUILD_PATH=".build/bgfx_ios_arm64_debug"
BGFX_IOS_SIMULATOR_RELEASE_BUILD_PATH=".build/bgfx_ios_simulator_release"
BGFX_IOS_SIMULATOR_DEBUG_BUILD_PATH=".build/bgfx_ios_simulator_debug"
BGFX_IOS_SIMULATOR64_RELEASE_BUILD_PATH=".build/bgfx_ios_simulator64_release"
BGFX_IOS_SIMULATOR64_DEBUG_BUILD_PATH=".build/bgfx_ios_simulator64_debug"
BGFX_OSX_RELEASE64_BUILD_PATH=".build/bgfx_osx_release64"
BGFX_OSX_DEBUG64_BUILD_PATH=".build/bgfx_osx_debug64"
BGFX_VS2017_RELEASE_BUILD_PATH=".build/bgfx_vs2017_release"
BGFX_VS2017_DEBUG_BUILD_PATH=".build/bgfx_vs2017_debug"

BREAKPAD_ANDROID_ARM_BUILD_PATH=".build/breakpad_android_arm"
BREAKPAD_ANDROID_ARM64_BUILD_PATH=".build/breakpad_android_arm64"
BREAKPAD_ANDROID_X86_BUILD_PATH=".build/breakpad_android_x86"

BREAKPAD_IOS_RELEASE_BUILD_PATH=".build/breakpad_ios_release"
BREAKPAD_IOS_DEBUG_BUILD_PATH=".build/breakpad_ios_debug"
BREAKPAD_IOS_SIMULATOR_RELEASE_BUILD_PATH=".build/breakpad_ios_simulator_release"
BREAKPAD_IOS_SIMULATOR_DEBUG_BUILD_PATH=".build/breakpad_ios_simulator_debug"

.PHONY: help

help:
	@echo "Specify build target. please read Makefile"

clean:
	@"$(MAKE)" -C bgfx clean
	@rm -rf .cache/
	@rm -rf breakpad/src/.build
	@rm -rf breakpad/src/src/client/ios/build

projgen:
	@"$(MAKE)" -C bgfx projgen-tools-lib

breakpad-android-arm-build-path:
	@echo $(BREAKPAD_ANDROID_ARM_BUILD_PATH)/

breakpad-android-arm: 
	@echo "Building NDK standalone toolchain"
	@bash .tools/build_ndk_standalone_toolchains.sh arm android-21 $(PWD)/.cache/ndk_standalone_toolchains/arm
	@echo "Building NDK breakpad standalone library"
	@bash .tools/build_ndk_breakpad.sh arm-linux-androideabi arm-linux-androideabi breakpad/src $(PWD)/.cache/ndk_standalone_toolchains/arm
	@mkdir -p $(BREAKPAD_ANDROID_ARM_BUILD_PATH)/
	@echo "Copy files from breakpad/src/.build/src/client/linux/* -> $(BREAKPAD_ANDROID_ARM_BUILD_PATH)/"
	@cp breakpad/src/src/src/client/linux/*.a $(BREAKPAD_ANDROID_ARM_BUILD_PATH)/

breakpad-android-arm64-build-path:
	@echo $(BREAKPAD_ANDROID_ARM64_BUILD_PATH)/

breakpad-android-arm64: 
	@echo "Building NDK standalone toolchain"
	@bash .tools/build_ndk_standalone_toolchains.sh arm64 android-21 $(PWD)/.cache/ndk_standalone_toolchains/arm64
	@echo "Building NDK breakpad standalone library"
	@bash .tools/build_ndk_breakpad.sh aarch64-linux-android aarch64-linux-android breakpad/src $(PWD)/.cache/ndk_standalone_toolchains/arm64
	@mkdir -p $(BREAKPAD_ANDROID_ARM64_BUILD_PATH)/
	@echo "Copy files from breakpad/src/.build/src/client/linux/* -> $(BREAKPAD_ANDROID_ARM64_BUILD_PATH)/"
	@cp breakpad/src/src/src/client/linux/*.a $(BREAKPAD_ANDROID_ARM64_BUILD_PATH)/

breakpad-android-x86-build-path:
	@echo $(BREAKPAD_ANDROID_X86_BUILD_PATH)/

breakpad-android-x86: 
	@echo "Building NDK standalone toolchain"
	@bash .tools/build_ndk_standalone_toolchains.sh x86 android-21 $(PWD)/.cache/ndk_standalone_toolchains/x86
	@echo "Building NDK breakpad standalone library"
	@bash .tools/build_ndk_breakpad.sh x86-linux-androideabi i686-linux-android breakpad/src $(PWD)/.cache/ndk_standalone_toolchains/x86
	@mkdir -p $(BREAKPAD_ANDROID_X86_BUILD_PATH)/
	@echo "Copy files from breakpad/src/.build/src/client/linux/* -> $(BREAKPAD_ANDROID_X86_BUILD_PATH)/"
	@cp breakpad/src/src/src/client/linux/*.a $(BREAKPAD_ANDROID_X86_BUILD_PATH)/

breakpad-ios-release-build-path:
	@echo $(BREAKPAD_IOS_RELEASE_BUILD_PATH)/

breakpad-ios-release:
	@echo "Building iOS breakpad release library"
	@bash .tools/build_ios_breakpad.sh breakpad/src iphoneos Release
	@echo "Copy files from breakpad/src/src/client/ios/build/Release-iphoneos/*.a -> $(BREAKPAD_IOS_RELEASE_BUILD_PATH)/"
	@mkdir -p $(BREAKPAD_IOS_RELEASE_BUILD_PATH)/
	@cp breakpad/src/src/client/ios/build/Release-iphoneos/*.a $(BREAKPAD_IOS_RELEASE_BUILD_PATH)/

breakpad-ios-debug-build-path:
	@echo $(BREAKPAD_IOS_DEBUG_BUILD_PATH)/

breakpad-ios-debug:
	@echo "Building iOS breakpad debug library"
	@bash .tools/build_ios_breakpad.sh breakpad/src iphoneos Debug
	@echo "Copy files from breakpad/src/src/client/ios/build/Debug-iphoneos/*.a -> $(BREAKPAD_IOS_DEBUG_BUILD_PATH)/"
	@mkdir -p $(BREAKPAD_IOS_DEBUG_BUILD_PATH)/
	@cp breakpad/src/src/client/ios/build/Debug-iphoneos/*.a $(BREAKPAD_IOS_DEBUG_BUILD_PATH)/

breakpad-ios-simulator-release-build-path:
	@echo $(BREAKPAD_IOS_SIMULATOR_RELEASE_BUILD_PATH)/

breakpad-ios-simulator-release:
	@echo "Building iPhone simulator breakpad release library"
	@bash .tools/build_ios_breakpad.sh breakpad/src iphonesimulator Release
	@echo "Copy files from breakpad/src/src/client/ios/build/Release-iphonesimulator/*.a -> $(BREAKPAD_IOS_SIMULATOR_RELEASE_BUILD_PATH)/"
	@mkdir -p $(BREAKPAD_IOS_SIMULATOR_RELEASE_BUILD_PATH)/
	@cp breakpad/src/src/client/ios/build/Release-iphonesimulator/*.a $(BREAKPAD_IOS_SIMULATOR_RELEASE_BUILD_PATH)/

breakpad-ios-simulator-debug-build-path:
	@echo $(BREAKPAD_IOS_SIMULATOR_DEBUG_BUILD_PATH)/

breakpad-ios-simulator-debug:
	@echo "Building iPhone simulator breakpad debug library"
	@bash .tools/build_ios_breakpad.sh breakpad/src iphonesimulator Debug
	@echo "Copy files from breakpad/src/src/client/ios/build/Debug-iphonesimulator/*.a -> $(BREAKPAD_IOS_SIMULATOR_DEBUG_BUILD_PATH)/"
	@mkdir -p $(BREAKPAD_IOS_SIMULATOR_DEBUG_BUILD_PATH)/
	@cp breakpad/src/src/client/ios/build/Debug-iphonesimulator/*.a $(BREAKPAD_IOS_SIMULATOR_DEBUG_BUILD_PATH)/

bgfx-android-arm-release:
	@"$(MAKE)" -C bgfx android-arm-release
	@mkdir -p $(BGFX_ANDROID_ARM_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/android-arm/bin/* -> $(BGFX_ANDROID_ARM_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/android-arm/bin/*
	@cp bgfx/.build/android-arm/bin/* $(BGFX_ANDROID_ARM_RELEASE_BUILD_PATH)/

bgfx-android-arm-release-build-path:
	@echo $(BGFX_ANDROID_ARM_RELEASE_BUILD_PATH)/
	
bgfx-android-arm-debug:
	@"$(MAKE)" -C bgfx android-arm-debug
	@mkdir -p $(BGFX_ANDROID_ARM_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/android-arm/bin/* -> $(BGFX_ANDROID_ARM_DEBUG_BUILD_PATH)/" 
	@ls -a bgfx/.build/android-arm/bin/*
	@cp bgfx/.build/android-arm/bin/* $(BGFX_ANDROID_ARM_DEBUG_BUILD_PATH)/

bgfx-android-arm-debug-build-path:
	@echo $(BGFX_ANDROID_ARM_DEBUG_BUILD_PATH)/
	
bgfx-android-x86-release:
	@"$(MAKE)" -C bgfx android-x86-release
	@mkdir -p $(BGFX_ANDROID_X86_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/android-x86/bin/* -> $(BGFX_ANDROID_X86_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/android-x86/bin/*
	@cp bgfx/.build/android-x86/bin/* $(BGFX_ANDROID_X86_RELEASE_BUILD_PATH)/

bgfx-android-x86-release-build-path:
	@echo $(BGFX_ANDROID_X86_RELEASE_BUILD_PATH)/

bgfx-android-x86-debug:
	@"$(MAKE)" -C bgfx android-x86-debug
	@mkdir -p $(BGFX_ANDROID_X86_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/android-x86/bin/* -> $(BGFX_ANDROID_X86_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/android-x86/bin/*
	@cp bgfx/.build/android-x86/bin/* $(BGFX_ANDROID_X86_DEBUG_BUILD_PATH)/

bgfx-android-x86-debug-build-path:
	@echo $(BGFX_ANDROID_X86_DEBUG_BUILD_PATH)/

bgfx-osx-release64:
	@"$(MAKE)" -C bgfx osx-release64
	@mkdir -p $(BGFX_OSX_RELEASE64_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/osx64_clang/bin/* -> $(BGFX_OSX_RELEASE64_BUILD_PATH)/" 
	@ls -a bgfx/.build/osx64_clang/bin/*
	@cp bgfx/.build/osx64_clang/bin/* $(BGFX_OSX_RELEASE64_BUILD_PATH)/

bgfx-osx-release64-build-path:
	@echo $(BGFX_OSX_RELEASE64_BUILD_PATH)/

bgfx-osx-debug64:
	@"$(MAKE)" -C bgfx osx-debug64
	@mkdir -p $(BGFX_OSX_DEBUG64_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/osx64_clang/bin/* -> $(BGFX_OSX_DEBUG64_BUILD_PATH)/" 
	@ls -a bgfx/.build/osx64_clang/bin/*
	@cp bgfx/.build/osx64_clang/bin/* $(BGFX_OSX_DEBUG64_BUILD_PATH)/

bgfx-osx-debug64-build-path:
	@echo $(BGFX_OSX_DEBUG64_BUILD_PATH)/

bgfx-ios-arm-release:
	@"$(MAKE)" -C bgfx ios-arm-release
	@mkdir -p $(BGFX_IOS_ARM_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-arm/bin/* -> $(BGFX_IOS_ARM_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-arm/bin/*
	@cp bgfx/.build/ios-arm/bin/* $(BGFX_IOS_ARM_RELEASE_BUILD_PATH)/

bgfx-ios-arm-release-build-path:
	@echo $(BGFX_IOS_ARM_RELEASE_BUILD_PATH)/

bgfx-ios-arm-debug:
	@"$(MAKE)" -C bgfx ios-arm-debug
	@mkdir -p $(BGFX_IOS_ARM_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-arm/bin/* -> $(BGFX_IOS_ARM_DEBUG_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-arm/bin/*
	@cp bgfx/.build/ios-arm/bin/* $(BGFX_IOS_ARM_DEBUG_BUILD_PATH)/

bgfx-ios-arm-debug-build-path:
	@echo $(BGFX_IOS_ARM_DEBUG_BUILD_PATH)/

bgfx-ios-arm64-release:
	@"$(MAKE)" -C bgfx ios-arm64-release
	@mkdir -p $(BGFX_IOS_ARM64_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-arm64/bin/* -> $(BGFX_IOS_ARM64_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-arm64/bin/*
	@cp bgfx/.build/ios-arm64/bin/* $(BGFX_IOS_ARM64_RELEASE_BUILD_PATH)/

bgfx-ios-arm64-release-build-path:
	@echo $(BGFX_IOS_ARM64_RELEASE_BUILD_PATH)/

bgfx-ios-arm64-debug:
	@"$(MAKE)" -C bgfx ios-arm64-debug
	@mkdir -p $(BGFX_IOS_ARM64_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-arm64/bin/* -> $(BGFX_IOS_ARM64_DEBUG_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-arm64/bin/*
	@cp bgfx/.build/ios-arm64/bin/* $(BGFX_IOS_ARM64_DEBUG_BUILD_PATH)/

bgfx-ios-arm64-debug-build-path:
	@echo $(BGFX_IOS_ARM64_DEBUG_BUILD_PATH)/

bgfx-ios-simulator-release:
	@"$(MAKE)" -C bgfx ios-simulator-release
	@mkdir -p $(BGFX_IOS_SIMULATOR_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-simulator/bin/* -> $(BGFX_IOS_SIMULATOR_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-simulator/bin/*
	@cp bgfx/.build/ios-simulator/bin/* $(BGFX_IOS_SIMULATOR_RELEASE_BUILD_PATH)/

bgfx-ios-simulator-release-build-path:
	@echo $(BGFX_IOS_SIMULATOR_RELEASE_BUILD_PATH)/

bgfx-ios-simulator-debug:
	@"$(MAKE)" -C bgfx ios-simulator-debug
	@mkdir -p $(BGFX_IOS_SIMULATOR_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-simulator/bin/* -> $(BGFX_IOS_SIMULATOR_DEBUG_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-simulator/bin/*
	@cp bgfx/.build/ios-simulator/bin/* $(BGFX_IOS_SIMULATOR_DEBUG_BUILD_PATH)/

bgfx-ios-simulator-debug-build-path:
	@echo $(BGFX_IOS_SIMULATOR_DEBUG_BUILD_PATH)/

bgfx-ios-simulator64-release:
	@"$(MAKE)" -C bgfx ios-simulator64-release
	@mkdir -p $(BGFX_IOS_SIMULATOR64_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-simulator64/bin/* -> $(BGFX_IOS_SIMULATOR64_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-simulator64/bin/*
	@cp bgfx/.build/ios-simulator64/bin/* $(BGFX_IOS_SIMULATOR64_RELEASE_BUILD_PATH)/

bgfx-ios-simulator64-release-build-path:
	@echo $(BGFX_IOS_SIMULATOR64_RELEASE_BUILD_PATH)/

bgfx-ios-simulator64-debug:
	@"$(MAKE)" -C bgfx ios-simulator64-debug
	@mkdir -p $(BGFX_IOS_SIMULATOR64_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/ios-simulator64/bin/* -> $(BGFX_IOS_SIMULATOR64_DEBUG_BUILD_PATH)/" 
	@ls -a bgfx/.build/ios-simulator64/bin/*
	@cp bgfx/.build/ios-simulator64/bin/* $(BGFX_IOS_SIMULATOR64_DEBUG_BUILD_PATH)/

bgfx-ios-simulator64-debug-build-path:
	@echo $(BGFX_IOS_SIMULATOR64_DEBUG_BUILD_PATH)/

bgfx-vs2017-release:
	@"$(MSBUILD)" bgfx/.build/projects/vs2017/bgfx.sln //p:Configuration=Debug
	@mkdir -p $(BGFX_VS2017_RELEASE_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/win32_vs2017/bin/* -> $(BGFX_VS2017_RELEASE_BUILD_PATH)/" 
	@ls -a bgfx/.build/win32_vs2017/bin/*
	@cp bgfx/.build/win32_vs2017/bin/* $(BGFX_VS2017_RELEASE_BUILD_PATH)/

bgfx-vs2017-release-build-path:
	@echo $(BGFX_VS2017_RELEASE_BUILD_PATH)/

bgfx-vs2017-debug:
	@"$(MSBUILD)" bgfx/.build/projects/vs2017/bgfx.sln //p:Configuration=Release
	@mkdir -p $(BGFX_VS2017_DEBUG_BUILD_PATH)/
	@echo "Copy files from bgfx/.build/win32_vs2017/bin/* -> $(BGFX_VS2017_DEBUG_BUILD_PATH)/" 
	@ls -a bgfx/.build/win32_vs2017/bin/*
	@cp bgfx/.build/win32_vs2017/bin/* $(BGFX_VS2017_DEBUG_BUILD_PATH)/

bgfx-vs2017-debug-build-path:
	@echo $(BGFX_VS2017_DEBUG_BUILD_PATH)/